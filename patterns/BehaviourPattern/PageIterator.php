<?php

/**
 * Iterator Design Pattern
 *
 * @author Florian Voigtländer
 * @since  09.09.2022
 */
class PageIterator implements Iterator
{
	private int $i_position = 0;
	private PageCollection $page_collection;

	public function __construct(PageCollection $page_collection)
	{
		$this->page_collection = $page_collection;
	}

	public function current()
	{
		return $this->page_collection->getPage($this->i_position);
	}

	public function key(): int
	{
		return $this->i_position;
	}

	public function next(): void
	{
		$this->i_position++;
	}

	public function rewind(): void
	{
		$this->i_position = 0;
	}

	public function valid(): bool
	{
		return !is_null($this->page_collection->getPage($this->i_position));
	}
}
