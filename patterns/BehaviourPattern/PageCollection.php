<?php

/**
 * Aggregator
 *
 * @author Florian Voigtländer
 * @since  09.09.2022
 */
class PageCollection implements IteratorAggregate
{
	private array $page_titles = array();

	public function getIterator(): PageIterator
	{
		return new PageIterator($this);
	}

	public function addPage(string $page_title): void
	{
		$this->page_titles[] = $page_title;
	}

	public function getPage(string $key): ?string
	{
		if (isset($this->page_titles[$key]))
			return $this->page_titles[$key];

		return null;
	}

	public function isEmpty(): bool
	{
		return empty($this->page_titles);
	}
}
