<?php

/**
 * Creational Pattern: Singleton Design Pattern
 * in this case a logger class
 *
 * @author Florian Voigtländer
 * @since  08.09.2022
 */
class Logger
{
    private static Logger $instance;

    private function __construct() {}

    public static function getInstance(): Logger
    {
        if (!isset(self::$instance)) {
            self::$instance = new self();
        }

        return self::$instance;
    }

		public function log(string $message): void
		{
			echo $message;
		}
}
