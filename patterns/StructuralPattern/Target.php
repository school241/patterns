<?php

namespace StructuralPattern;

class Target
{
    public function request(): string
    {
        return "Target: The defaults targets behaviour.";
    }
}