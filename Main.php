<?php

use StructuralPattern\Program;

require_once 'patterns/creational_patterns/Logger.php';

/**
 * Main class
 *
 * @author Florian Voigtländer
 * @since  08.09.2022
 */
class Main
{
	/**
	 * example of pattern usage
	 */
	public function main(): void
	{
		// Singleton
		$logger = Logger::getInstance();
		$logger->log('message');

		// Adapter
		$program = new Program();
		$target = $program->helperFunction();
		$program->clientCode($target);

		// Iterator
		$page_collection = new PageCollection();
		$page_collection->addPage('Page 1');
		$page_collection->addPage('Page 2');

		foreach ($page_collection as $page)
			var_dump($page);
	}
}
